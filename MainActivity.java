package com.example.intern.oop;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    int pos = 0;
    final ArrayList<String> al = new ArrayList<>();
    Button btnAdd, btnUpdate, btnDelete;
    EditText edt;
    ListView lv;
    ArrayAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getControll();

        al.add("Android");
        al.add("Web");
        al.add("QA");

        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, al);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                edt.setText(al.get(position));
                pos = position;
                Log.i("position",pos+"");
            }
        });

        btnAdd.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
    }

    public void getControll(){
        lv = (ListView) findViewById(R.id.listView);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        edt = (EditText) findViewById(R.id.editText);
    }

    public void add(){
        String name = edt.getText().toString();
        al.add(name);
    }

    public void update(String a, int id){
        al.set(id, a);
    }

    public void delete(int id){
        al.remove(id);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAdd:
                add();
                adapter.notifyDataSetChanged();
                break;
            case R.id.btnUpdate:
                Log.i("pos in update",pos+"");
                update(edt.getText().toString(), pos);
                adapter.notifyDataSetChanged();

                break;
            case R.id.btnDelete:
                delete(pos);
                adapter.notifyDataSetChanged();
                break;
        }
    }
}
