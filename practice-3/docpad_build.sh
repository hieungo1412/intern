docker build -t docpad-docker .
docker run -it --rm -w /home/app -v $(pwd):/home/app docpad-docker docpad init
docker run -it --rm -w /home/app -v $(pwd):/home/app docpad-docker bower init
docker run -it --rm -w /home/app -v $(pwd):/home/app docpad-docker bower install bootstrap-sass --save
docker run -it --rm -w /home/app -v $(pwd):/home/app docpad-docker bower install jquery --save
docker run -it --rm -w /home/app -v $(pwd):/home/app docpad-docker docpad install livereload
docker run -it --rm -w /home/app -v $(pwd):/home/app docpad-docker docpad install pug
docker run -it --rm -w /home/app -v $(pwd):/home/app docpad-docker docpad install sass
